import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CuerpoComponent } from './components/cuerpo/cuerpo.component';
import { DirectivaDirective } from './directivas/directiva.directive';

@NgModule({
  declarations: [
    AppComponent,
    CuerpoComponent,
    DirectivaDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
